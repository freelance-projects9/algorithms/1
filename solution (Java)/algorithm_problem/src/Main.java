import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("(1) Brute force.\n(2) Dynamic programming.\nYour choice: ");
        String choice = scanner.nextLine();
        if (choice.equals("1")) {
            BruteForce bruteForce = new BruteForce();
            bruteForce.run();
        } else {
            DynamicProgramming dynamicProgramming = new DynamicProgramming();
            dynamicProgramming.run();
        }
    }
}
