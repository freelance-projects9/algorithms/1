import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class BruteForce {
    int n;
    float best = 0;
    boolean[] bestSelected;
    Node[] nodes;

    void run() {

        /**
         *     Reading input from file
         */
        ArrayList<String> lines = new ArrayList<>();             // stores input lines from user
        try {
            BufferedReader br = new BufferedReader(new FileReader("src\\Example.txt"));
            String line = br.readLine();
            while (line != null) {
                lines.add(line);
                line = br.readLine();
            }
            n = lines.size();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // initializing variables
        nodes = new Node[n + 1];                    // stores all nodes of the tree
        boolean[] selected = new boolean[n + 1];    // stores a temporary solution
        bestSelected = new boolean[n + 1];          // stores the optimal solution


        /**
         *    start building the tree by parsing the input
         */
        nodes[0] = new Node();
        nodes[0].childrenIDs = new ArrayList<>();
        nodes[0].id = 0;
        for (int i = 1; i < n + 1; i++) {
            Node node = new Node();
            node.childrenIDs = new ArrayList<>();
            String[] words = lines.get(i - 1).split(":");
            node.parentID = Integer.parseInt(words[0]);
            node.name = words[1];
            node.id = Integer.parseInt(words[2]);
            node.evaluation = Float.parseFloat(words[3]);
            nodes[i] = node;
            nodes[node.parentID].childrenIDs.add(node.id);
//            System.out.println("id: " + node.id + " parent: " + node.parentID + " eval: " + node.evaluation + " name: " + node.name);
        }

        /**
         *         BRUTE FORCE MTEHOD
         */
        bruteForce(selected, 1, 0);

        /**
         *         Prints results
         */
        System.out.println("ID Name");
        for (int i = 0; i < n + 1; i++) {
            if (bestSelected[i]) {
                System.out.println("" + nodes[i].id + "  " + nodes[i].name);
            }
        }
        System.out.println(best);
    }

    void bruteForce(boolean[] selected, int i, float eval) {
        if (i == n + 1) {
            if (eval > best) {
                best = eval;
                bestSelected = selected.clone();
            }
        } else {
            if (selected[nodes[i].parentID]) {  // can't select current node
                bruteForce(selected, i + 1, eval);
            } else {                               // We can either select current node or not
                // 1. select current node
                selected[i] = true;
                eval += nodes[i].evaluation;
                bruteForce(selected, i + 1, eval);
                // 2. unselect current node
                selected[i] = false;
                eval -= nodes[i].evaluation;
                bruteForce(selected, i + 1, eval);
            }
        }

    }

}
