import java.util.ArrayList;

class Node {
    int parentID;
    int id;
    float evaluation;
    String name;
    ArrayList<Integer> childrenIDs; // we need them in dynamicProgramming method only
}

