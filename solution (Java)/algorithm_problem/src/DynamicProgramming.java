import java.io.*;
import java.util.ArrayList;

public class DynamicProgramming {
    int n;
    float best = 0;
    boolean[] bestSelected;
    Node[] nodes;

    // for dynamic method
    float[] evalIfSelected;
    float[] evalIfNotSelected;
    ArrayList<Integer>[] solutionsIfNotSelected;
    ArrayList<Integer>[] solutionsIfSelected;


    void run() {

        /**
         *     Reading input from file
         */
        ArrayList<String> lines = new ArrayList<>();             // stores input lines from user
        try {
            BufferedReader br = new BufferedReader(new FileReader("src\\Example.txt"));
            String line = br.readLine();
            while (line != null) {
                lines.add(line);
                line = br.readLine();
            }
            // Find the value of [n] variable
            n = lines.size();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // initializing variables
        nodes = new Node[n + 1];                    // stores all nodes of the tree

        evalIfSelected = new float[n + 1];        // stores the result of method call on a node, when the node is selected
        evalIfNotSelected = new float[n + 1];      // stores the result of method call on a node, when the node is unselected

        solutionsIfNotSelected = new ArrayList[n + 1];
        solutionsIfSelected = new ArrayList[n + 1];

        // initialize results with -1
        for (int i = 0; i < n + 1; i++) {
            solutionsIfNotSelected[i]= new ArrayList<>();
            solutionsIfSelected[i]= new ArrayList<>();

            evalIfSelected[i] = -1;
            evalIfNotSelected[i] = -1;
        }

        /**
         *    start building the tree by parsing the input
         */
        nodes[0] = new Node();
        nodes[0].childrenIDs = new ArrayList<>();
        nodes[0].id = 0;
        for (int i = 1; i < n + 1; i++) {
            Node node = new Node();
            node.childrenIDs = new ArrayList<>();
            String[] words = lines.get(i - 1).split(":");
            node.parentID = Integer.parseInt(words[0]);
            node.name = words[1];
            node.id = Integer.parseInt(words[2]);
            node.evaluation = Float.parseFloat(words[3]);
            nodes[i] = node;
            nodes[node.parentID].childrenIDs.add(node.id);
//            System.out.println("id: " + node.id + " parent: " + node.parentID + " eval: " + node.evaluation + " name: " + node.name);
        }

        /**
         *         DYNAMIC PROGRAMMING METHOD
         */

        // we have two cases for the root node
        float eval1 = dynamicProgramming(false, nodes[1]);
        float eval2 = dynamicProgramming(true, nodes[1]);

        ArrayList<Integer> bestSolution;
        if (eval2 > eval1) {
            solutionsIfSelected[1].add(1);
            bestSolution = solutionsIfSelected[1];
        } else {
            bestSolution = solutionsIfNotSelected[1];
        }

        float bestEvaluation = Math.max(eval1, eval2);

        /**
         *         Prints results
         */
        System.out.println("ID Name");

        for (int i = 0; i < n + 1; i++) {
            if (bestSolution.contains(i)) {
                System.out.println("" + nodes[i].id + "  " + nodes[i].name);
            }
        }
        System.out.println(bestEvaluation);

    }


    float dynamicProgramming(boolean isSelected, Node node) {
        if (isSelected) {
            if (evalIfSelected[node.id] != -1) {
                return evalIfSelected[node.id];
            }
            if (node.childrenIDs == null) {
                evalIfSelected[node.id] = node.evaluation;
                return node.evaluation;
            } else {
                float eval = node.evaluation;
                for (int childID : node.childrenIDs) {
                    eval += dynamicProgramming(false, nodes[childID]);
                    solutionsIfSelected[node.id].addAll(solutionsIfNotSelected[childID]);
                }
                evalIfSelected[node.id] = eval;
                return eval;
            }
        } else {
            if (evalIfNotSelected[node.id] != -1) {
                return evalIfNotSelected[node.id];
            }
            if (node.childrenIDs == null) {
                evalIfNotSelected[node.id] = 0;
                return 0;
            } else {
                float eval = 0;
                float eval1, eval2;
                for (int childID : node.childrenIDs) {
                    eval1 = dynamicProgramming(false, nodes[childID]);
                    eval2 = dynamicProgramming(true, nodes[childID]);
                    if (eval2 > eval1) {
                        solutionsIfNotSelected[node.id].addAll(solutionsIfSelected[childID]);
                        solutionsIfNotSelected[node.id].add(childID);
                    }else{
                        solutionsIfNotSelected[node.id].addAll(solutionsIfNotSelected[childID]);
                    }
                    eval += Math.max(eval1, eval2);
                }
                evalIfNotSelected[node.id] = eval;
                return eval;
            }
        }
    }


}
