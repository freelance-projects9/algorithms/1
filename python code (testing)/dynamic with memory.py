nodes = {}
n = 7

resultsPositive = {}
resultsNegative = {}

counter = 0

def dynamic_programming(selected, root):
    global counter
    if selected:
        if root in resultsPositive:
            return resultsPositive[root]
        if not nodes[root][1]:
            
            resultsPositive[root] = nodes[root][0][3]
            return resultsPositive[root]
        else:
            eval = nodes[root][0][3]
            # names = [nodes[root][0][1]]
            for child in nodes[root][1]:
                counter += 1
                eval += dynamic_programming(False, child)
            resultsPositive[root] = eval
            return eval
    else:
        if root in resultsNegative:
            return resultsNegative[root]
        if not nodes[root][1]:
            
            resultsNegative[root] = 0
            return resultsNegative[root]
        else:
            eval = 0
            for child in nodes[root][1]:
                counter += 2
                eval1 = dynamic_programming(False, child)
                eval2 = dynamic_programming(True, child)
                eval += max(eval1, eval2)
            resultsNegative[root] = eval
            return resultsNegative[root]

lines = []
for i in range(n):
    lines.append(input("enter node " + str(i) + ": "))


for line in lines:
    words = line.split(":")
    id_of_parent = int(words[0])
    name = words[1]
    id = int(words[2])
    eval_score = float(words[3])
    node_info = [id_of_parent, name, id, eval_score]
    print("{}:{}:{}:{}".format(id_of_parent,name,id,eval_score))
    if id not in nodes:
        nodes[id] = {}
    nodes[id][0] = node_info
    nodes[id][1] = []
    if id_of_parent not in nodes:
        nodes[id_of_parent] = {}
        nodes[id_of_parent][1] = [id]
    else:
        nodes[id_of_parent][1].append(id)

print(nodes)
 
eval1 = dynamic_programming(False, 1)
eval2 = dynamic_programming(True, 1)
print(max(eval1, eval2))

print("Counter =" + str(counter))