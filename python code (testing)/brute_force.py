nodes = {}

def print_tree(nodes):
    for parent in nodes:
        for child in nodes[parent]:
            print(child)


def brute_force(node_info, take, solution,result):
    if take:
        solution.append(node_info[1][1])
        result += node_info[1][3]
        if node_info[0] not in nodes:
            print(str(solution) + str(result))
            return result
        for child in nodes[node_info[0]]:
            result += brute_force(child, False, solution,result)
        
    else:
        if node_info[0] not in nodes:
            print(str(solution) + str(result))
            return result
        for child in nodes[node_info[0]]:
            r1 = brute_force(child, True, solution,result)
            solution = solution[:-1]
            r2 = brute_force(child, False, solution,result)
            result += max(r1,r2)
    return result

n = 15
lines = []
for i in range(n):
    lines.append(input("enter node " + str(i) + ": "))

for line in lines:
    words = line.split(":")
    print(words)
    id_of_parent = int(words[0])
    name = words[1]
    id = int(words[2])
    eval_score = float(words[3])
    node_info = [id_of_parent, name, id, eval_score]
    print("{}:{}:{}:{}".format(id_of_parent,name,id,eval_score))
    if id_of_parent in nodes:
        nodes[id_of_parent].append((id,node_info)) 
    else:
        nodes[id_of_parent] = [(id,node_info)]
print_tree(nodes)

result1 = brute_force(nodes[0][0], True, [],0)
result2 = brute_force(nodes[0][0],False, [],0)
print(result1)
print(result2)