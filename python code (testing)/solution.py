nodes = {}
solutions = {}
costs = []
n = 7
best = 0

first_call = 0
second_call = 0

counter = 0
def brute_force(selected, i, cost):
    global counter, first_call, second_call
    global best
    if i == n + 1:
        # print(cost)
        if cost > best:
            best = cost
            print(best)
        # costs.append(cost)
        # if cost not in solutions:
        #     solutions[cost] = []
            solution = []
            for i in range(n + 1):
                if selected[i]:
                    solution.append((nodes[i][1],nodes[i][3]))
            print(solution)
        # solutions[cost].append(solution)
        counter += 1
        # print(counter)
    else:
        if selected[nodes[i][0]]:
            first_call += 1
            # can't select current node
            brute_force(selected, i+1, cost)
        else:
            second_call += 2
            # select current node
            selected[i] = True
            cost += nodes[i][3]
            brute_force(selected, i+1, cost)
            # don't select current node
            selected[i] = False
            cost -= nodes[i][3]            
            brute_force(selected, i+1, cost)


lines = []
for i in range(n):
    lines.append(input("enter node " + str(i) + ": "))


for line in lines:
    words = line.split(":")
    id_of_parent = int(words[0])
    name = words[1]
    id = int(words[2])
    eval_score = float(words[3])
    node_info = [id_of_parent, name, id, eval_score]
    print("{}:{}:{}:{}".format(id_of_parent,name,id,eval_score))
    nodes[id] = node_info
 

selected = []
for i in range(n + 1):
    selected.append(False)

brute_force(selected, 1, 0 )

# print(max(costs))
# best = solutions[max(costs)]
print(best)

print("Counter =" + str(counter))
print('first '+ str(first_call))
print('second ' + str(second_call))